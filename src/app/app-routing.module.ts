import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ProductsGuard as ProductsGuard } from './shared/guards/products.guard';
import { AuthService } from './shared/services/auth.service';
import { ProductsResolver } from './shared/resolvers/products.resolver';
import { ProductsService } from './shared/services/products.service';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'products',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./login/login.module').then((mod) => mod.LoginModule),
  },
  {
    path: 'products',
    loadChildren: () =>
      import('./products/products.module').then((mod) => mod.ProductsModule),
    canActivate: [ProductsGuard],
    //resolve: { response: ProductsResolver },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), SharedModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services/auth.service';
import { Store } from '@ngrx/store'
import * as AuthActions from '../store/actions/auth.actions';
import * as ProductsActions from '../store/actions/products.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(private router: Router, private store: Store) {}

  ngOnInit(): void {}

  logMeIn() {
    this.store.dispatch(AuthActions.Authenticate());
    this.store.dispatch(ProductsActions.FetchProducts())
    this.router.navigate(['/products']);
  }
}

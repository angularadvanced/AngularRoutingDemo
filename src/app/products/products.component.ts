import { takeUntil, Subject } from 'rxjs';
import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {IProduct} from '../models/product';
import {ProductsService} from '../shared/services/products.service';
import { Store, select } from '@ngrx/store';
import * as ProductsReducer from "../../app/store/reducers/products.reducer";
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, OnDestroy {

  public products: IProduct [] = [];

  private unsubscribe$ = new Subject<void>();

  constructor(private activatedRoute: ActivatedRoute, private store: Store) { }

  ngOnInit(): void {
    this.store.pipe(
      select(ProductsReducer.getProducts),
      takeUntil(this.unsubscribe$)
    ).subscribe(products => this.products = products);

  }

  ngOnDestroy(): void {
    this.unsubscribe$.complete();
  }

}

import { TestBed } from '@angular/core/testing';

import { ProductsGuard } from './products.guard';

describe('ProductsGuardService', () => {
  let service: ProductsGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductsGuard);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

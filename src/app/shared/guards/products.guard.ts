import { Injectable, OnDestroy, OnInit } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { map, Observable, Subject, takeUntil } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { Store, select } from '@ngrx/store';
import * as authReducer from 'src/app/store/reducers/auth.reducer';

@Injectable()
export class ProductsGuard implements CanActivate {
  private isAuthenticated?: boolean;

  constructor(
    private router: Router,
    private authService: AuthService,
    private store: Store
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    this.store
      .pipe(select(authReducer.getAuth))
      .subscribe((isAuthenticated) => (this.isAuthenticated = isAuthenticated));

    if (!!this.isAuthenticated) {
      return this.isAuthenticated;
    } else {
      alert('User is not logged in ! You will be redirected to Login page');
      return this.router.navigate(['/login']);
    }
  }
}

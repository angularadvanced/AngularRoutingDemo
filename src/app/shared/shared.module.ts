import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsService } from './services/products.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { ProductsResolver } from './resolvers/products.resolver';
import { ProductsGuard } from './guards/products.guard';

@NgModule({
  declarations: [],
  imports: SharedModule.MODULE_LIST,
  exports: SharedModule.MODULE_LIST,
  //providers: [ProductsService, AuthService]
  providers: [
    ProductsGuard,
    ProductsService,
    AuthService,
    ProductsResolver,
    ProductsService,
  ],
})
export class SharedModule {
  static readonly MODULE_LIST = [CommonModule, HttpClientModule];
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class AuthService {

  isAuthenticated$ = new BehaviorSubject<boolean>(false);

  constructor() { }

  public isCurrentUserAuthenticated(): boolean {
    return this.isAuthenticated$.value;
  }

  public authenticate() {
    this.isAuthenticated$.next(true);
  }
}

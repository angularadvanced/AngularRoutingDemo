import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IProduct} from '../../models/product';
import {UrlManager} from '../../models/url-manager';
import {SharedModule} from '../shared.module';

@Injectable()
export class ProductsService {

  constructor(private httpClient: HttpClient) { }

  public getMyProducts(): Observable<any> {
    return this.httpClient.get<any>(UrlManager.PRODUCTS_ENDPOINT);
  }
}

import { Injectable, OnInit } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { catchError, Observable, of, take, map } from 'rxjs';
import { IProduct } from 'src/app/models/product';
import { ProductsService } from '../services/products.service';
import { Store, select } from '@ngrx/store';
import * as ProductsReducer from '../../store/reducers/products.reducer';
@Injectable()
export class ProductsResolver implements Resolve<IProduct[]> {
  constructor(private productsService: ProductsService) {}

  private products: IProduct[] = [];

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): IProduct[] | Observable<IProduct[]> | Promise<IProduct[]> {
    return this.productsService.getMyProducts().pipe(
      catchError(error => {
        return of(error);
      })
    );
  }
}

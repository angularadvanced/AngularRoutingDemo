import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { IProduct } from 'src/app/models/product';

export const FetchProducts = createAction(
  '[Products] - Fetch Products from API'
);

export const FetchProductsSuccess = createAction(
  '[Products] - Successfully Fetched Products from API',
  props<{ products: IProduct[] }>()
)

export const FetchProductsFailure = createAction(
  '[Products] - Failure when fetching Products from API',
  props<{ error: HttpErrorResponse }>()
)


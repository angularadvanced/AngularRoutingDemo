import { AuthState } from '../states/app.states';
import {
  createReducer,
  on,
  Action,
  createFeatureSelector,
  createSelector,
} from '@ngrx/store';
import * as AuthActions from '../actions/auth.actions';

export const initialAuthState: AuthState = {
  isAuthenticated: false,
};

const _authReducer = createReducer(
  initialAuthState,
  on(AuthActions.Authenticate, (state) => {
    return { ...state, isAuthenticated: true };
  })
);

export function authReducer(state: AuthState | undefined, action: Action) {
  return _authReducer(state, action);
}

export const getAuthState = createFeatureSelector<AuthState>('authState');

export const getAuth = createSelector(
  getAuthState,
  (state: AuthState) => state.isAuthenticated
);

import { ActionReducerMap } from "@ngrx/store";
import { AppState } from "../states/app.states";
import { authReducer } from "./auth.reducer";
import { productsReducer } from "./products.reducer";

export const reducers: ActionReducerMap<AppState> = {
  authState: authReducer,
  productsState: productsReducer
}

import { ProductsState } from '../states/app.states';
import {
  createReducer,
  on,
  Action,
  createFeatureSelector,
  createSelector,
} from '@ngrx/store';
import * as ProductsActions from '../actions/products.actions';

export const initialProductsState: ProductsState = {
  products: [],
  error: undefined,
};

const _productsReducer = createReducer(
  initialProductsState,
  on(ProductsActions.FetchProductsSuccess, (state, { products }) => {
    return { ...state, products: products };
  }),
  on(ProductsActions.FetchProductsFailure, (state, { error }) => {
    return { ...state, error: error };
  }),
);

export function productsReducer(state: ProductsState | undefined, action: Action) {
  return _productsReducer(state, action);
}


export const getProductsState = createFeatureSelector<ProductsState>('productsState');

export const getProducts = createSelector(
  getProductsState,
  (state: ProductsState) => state.products
);

export const getCallProductsApiError = createSelector(
  getProductsState,
  (state: ProductsState) => state.error
);

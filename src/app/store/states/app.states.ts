import { HttpErrorResponse } from "@angular/common/http";
import { IProduct } from "src/app/models/product";

export interface AppState {
  authState: AuthState;
  productsState: ProductsState;
}

export interface AuthState {
  isAuthenticated: boolean;
}


export interface ProductsState {
  products: IProduct[];
  error: any;
}

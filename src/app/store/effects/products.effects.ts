import { Injectable } from '@angular/core';
import { ProductsService } from 'src/app/shared/services/products.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {catchError, map, mergeMap, Observable, of, switchMap} from 'rxjs';
import { Action } from '@ngrx/store';
import * as ProductsAction from '../actions/products.actions';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class ProductsEffects {
  constructor(
    private productsService: ProductsService,
    private actions$: Actions
  ) {}

  FetchProducts$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(ProductsAction.FetchProducts),
      switchMap(action =>
        this.productsService.getMyProducts().pipe(
            map((resp) => ProductsAction.FetchProductsSuccess({products: resp.products})),
            catchError((error: HttpErrorResponse) => { return of(ProductsAction.FetchProductsFailure({error: error}))})
          )
      )
    )
  );
}
